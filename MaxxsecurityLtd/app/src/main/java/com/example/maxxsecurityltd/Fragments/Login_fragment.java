package com.example.maxxsecurityltd.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.example.maxxsecurityltd.R;
import com.google.android.material.textfield.TextInputLayout;

public class Login_fragment extends Fragment {
    TextInputLayout phone_number,password;
    Button login,sign_up;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_login_fragment, container, false);
       phone_number = view.findViewById(R.id.phonenumber);
       password = view.findViewById(R.id.password);
       login = view.findViewById(R.id.login);
       sign_up = view.findViewById(R.id.sign_up);

       sign_up.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               proceedToSignUp();
           }
       });
       login.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               proceedToDashboard();
           }
       });


       return view;
    }

    private void proceedToDashboard() {
        try {
            NavHostFragment.findNavController(this).navigate(R.id.action_login_fragment_to_signupfragment);

        }catch (Exception e){}
    }

    private void proceedToSignUp() {
        try {

        }catch (Exception e){

        }
    }
}