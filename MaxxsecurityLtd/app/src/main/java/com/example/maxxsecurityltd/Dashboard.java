package com.example.maxxsecurityltd;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.maxxsecurityltd.Fragments.Calls;
import com.example.maxxsecurityltd.Fragments.Login_fragment;
import com.example.maxxsecurityltd.Fragments.fragment_dashboard;
import com.example.maxxsecurityltd.Fragments.signupfragment;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

public class Dashboard extends AppCompatActivity {
    ChipNavigationBar chipNavigationBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dashboard);
        chipNavigationBar = findViewById(R.id.chip_navigationbar);
        chipNavigationBar.setItemSelected(R.id.bottomnav_dashboard, true);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new fragment_dashboard()).commit();
        bottomchip();
    }

    private void bottomchip() {
        chipNavigationBar.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int i) {
                Fragment fragment = null;
                switch (i) {
                    case R.id.bottomnav_dashboard:
                        fragment = new fragment_dashboard();
                        break;

                    case R.id.bottomnav_profile:
                        fragment = new Login_fragment();
                        break;

                    case R.id.bottomnav_emergency:
                        fragment = new Calls();
                        break;

                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
            }
        });
    }
}